from flask import Flask,render_template
import pymongo
import json
from pymongo import MongoClient
from jinja2 import Environment, PackageLoader
from flask_pymongo import PyMongo
#env = Environment(loader = PackageLoader('__name__','templates'))

app = Flask(__name__)
mongo = PyMongo(app)

connection = MongoClient();
db = connection['osndata']



@app.route("/")
def homepage():
	title = "Precog Summer 2017"

	return render_template("index.html",title=title)


@app.route('/type')
def type():
	type1 = db.tweets.find({"entities.media.type":{'$eq': "photo"}}).count()
	type2 = db.tweets.find({"entities.media.type":{'$eq': None}}).count()
	type3 = db.tweets.count()
	title = "Type of tweets"
	paragraph = ["Popularity metric is taken as according to the User Mention and Hashtags used by people"]
	pageType = 'popular'
	return render_template("type.html",type1=type1,type2=type2,title=title,paragraph=paragraph,pageType=pageType)

@app.route('/tweets')
def tweets():
	tt = db.tweets.count()
	twt = db.tweets.find({"retweeted_status":{'$ne': None}}).count()
	rt = db.tweets.find({"retweeted_status":{'$eq': None}}).count()
	tt1 = db.OCT31.count()
	twt1 = db.OCT31.find({"retweeted_status":{'$ne': None}}).count()
	rt1 = db.OCT31.find({"retweeted_status":{'$eq': None}}).count()
	tt2 = db.nov1.count()
	twt2 = db.nov1.find({"retweeted_status":{'$ne': None}}).count()
	rt2 = db.nov1.find({"retweeted_status":{'$eq': None}}).count()
	tt3 = db.nov2.count()
	twt3 = db.nov2.find({"retweeted_status":{'$ne': None}}).count()
	rt3 = db.nov2.find({"retweeted_status":{'$eq': None}}).count()
	tt4 = db.nov3.count()
	twt4 = db.nov3.find({"retweeted_status":{'$ne': None}}).count()
	rt4 = db.nov3.find({"retweeted_status":{'$eq': None}}).count()
	tt5 = db.nov4.count()
	twt5 = db.nov4.find({"retweeted_status":{'$ne': None}}).count()
	rt5 = db.nov4.find({"retweeted_status":{'$eq': None}}).count()
	title = "Original tweets vs Retweeted Tweets"
	paragraph = ["5 Days Data shows the following result "]
	pageType = 'tweets'
	return render_template("tweets.html",title=title,paragraph=paragraph,pageType=pageType,twt=twt,
		twt1=twt1,twt2=twt2,twt3=twt3,twt4=twt4,twt5=twt5,rt=rt,rt1=rt1,rt2=rt2,rt3=rt3,rt4=rt4,rt5=rt5,tt=tt,tt1=tt1,tt2=tt2,
		tt3=tt3,tt4=tt4,tt5=tt5)

@app.route('/popular')
def popular():
	usrmh = db.tweets.find({"entities.user_mentions.screen_name":{'$eq': "HillaryClinton"}}).count()
	usrmt = db.tweets.find({"entities.user_mentions.screen_name":{'$eq': "realDonaldTrump"}}).count()
	paragraph = ["Popularity Matric is taken according to the ratio of User Mention of Hillary and Trump in the Tweets"]
	pageType = 'popularity'
	return render_template("popular.html",paragraph = paragraph,pageType = pageType,usrmt =usrmt,usrmh=usrmh)

@app.route('/hashtags')
def hashatgs():
	mydic={}
	dic={}
	hash = db.tweets.find({"entities.hashtags.text":{'$ne':None}})
	for data in hash:
		a = data['entities']
		b= a['hashtags']
		for x in b:
			c=x['text']
			if not mydic.has_key(c):
				mydic[c] = 1
			if mydic.has_key(c):
				mydic[c]= mydic[c]+1;
	#dic = dict(sorted(mydic.iteritems(),key = operator.itemgetter(1),reverse = True)[:10])
		#for k in sorted(mydic.values()):
		#print mydic.keys()[mydic.values().index(k)], k
	dic = sorted(mydic, key=mydic.get, reverse=True)
	dic = dic[0:10]

	return render_template("hashtags.html",hash1=dic)


if __name__ == "__main__":
	app.run(debug = True,host='0.0.0.0',port=8080,passthrough_errors=True)
