Analysis on US Presidential Elections from a text point of view on the 12,000 curated tweets collected via the Streaming API by
filtering the stream for #USelections are stored in MongoDB collection.
From the curated tweets,I make a simple Web Application in python framework Flask which showcases the following:
● Locations of the Tweet.
● Who is more popular, Hillary or Trump?
● List of Top 10 Hashtags being used in the stream
● Distribution of Original Tweets vs Retweeted Tweets
● Distribution of favorite counts on Original Tweets
● Distribution of Type of Tweet i.e. Text, Image, Text+Image
